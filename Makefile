###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01a - Hello World - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a Hello World C program
###
### @author Ryan Edquiba 
### @date   1/13/22 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatGuy: crazyCatLady.cpp
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o

